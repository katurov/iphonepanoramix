# README #

This small project creates simple programmable panoramic head with RPi as a core. I suppose to use it in different projects such as: 

* Slow-motion filming in sport actions 
* Panoramas capturing in plain or with duplicated objects 
* Time-lapses

### What you will need for this project? ###

* Raspberry Pi (any with GPIO)
* Stepper motor 28BYJ-48 with driver on ULN2003
* LED Matrix MAX7219

### How do I get set up? ###

Checkout wiki page: https://bitbucket.org/katurov/iphonepanoramix/wiki/Home