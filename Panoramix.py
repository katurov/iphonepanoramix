# -*- coding: utf-8 -*-

import sys
import time
import math
import RPi.GPIO as GPIO

# Import library. Download library here: https://github.com/tutRPi/multilineMAX7219
import multilineMAX7219 as LEDMatrix	

# Import fonts
from multilineMAX7219_fonts import CP437_FONT, SINCLAIRS_FONT, LCD_FONT, TINY_FONT

# The following imported variables make it easier to feed parameters to the library functions
from multilineMAX7219 import DIR_L, DIR_R, DIR_U, DIR_D
from multilineMAX7219 import DIR_LU, DIR_RU, DIR_LD, DIR_RD
from multilineMAX7219 import DISSOLVE, GFX_ON, GFX_OFF, GFX_INVERT

reload(sys)  
sys.setdefaultencoding('utf8')

StepPinsX 	= [12,16,20,21]		# This is StepMotor's pins

								# Define advanced sequence
								# as shown in manufacturers datasheet
Seq 		= [[1,0,0,1],[1,0,0,0],[1,1,0,0],[0,1,0,0],[0,1,1,0],[0,0,1,0],[0,0,1,1],[0,0,0,1]]
StepCount 	= len(Seq)			# This will used in move head as an iterator' part

BEEPpin		= 4					# BEEPer pin
BUTTONpin 	= 17				# BUTTON pin
KeepSilence = True				# This is to patch button listener


# This returns a program for moving (later will made out from file)
#
# Used order: NAME, DIRECTION, ANGLE, SPEED
# If direction == 0 will make a countdown using angle as a timer
# Note that direction "-1" is clockwise
def readEntireProgram () :	
	return [
		["A", 0, 3, 1],
		["B", 1, 250, 1],
		["C", 1, 270, 1],
		["D", 1, 270, 1],
		["E", 1, 270, 1],
		["F", 1, 270, 1],
		["G", 0, 3, 1],
		["H", 1, 270, 1]
		]

# This will show small notes on MAX7219 LED Matrices
#
# If delay is int this will cause pause in program execution. When pause ends here is
#	the beep signal (human' signal)
def updateDisplay ( program, delay = None ) :
	
	if program == "done" or delay is None:
		LEDMatrix.static_message( program )
		return
	
	for xdelay in xrange( delay ) :
		if (delay-xdelay) > 9 :
			LEDMatrix.static_message(".")
			time.sleep(0.5)
			LEDMatrix.static_message(" ")
		else :
			LEDMatrix.static_message("{:d}".format(int(delay-xdelay)))
			time.sleep(0.5)

		time.sleep(0.5)
	
	GPIO.output(BEEPpin, 0)
	time.sleep(0.3)
	GPIO.output(BEEPpin, 1)
	
	return
		
# This moves the head
#
# StepDir 	- rotation direction: 1 is counter clock wise, -1 is clock wise 
# Angle 	- the angle to rotate (90, 180, 360 and so on, for 100000000 will just rotate)
# Speed 	- fastest is 1, slowest is... no matter: 4096 for full rotation with pause in msec 
def moveHead (StepDir = -1, Angle = 1, Speed = 1) :
	try :
		# Initialise variables
		StepCounter = 0
		if Speed < 1 or int(Speed) is None : Speed = 1
		WaitTime = int(Speed)/float(1000)
		RotationSteps = int ( (4096/360) * float(Angle) )				# Where 4096 steps are full rotation
		RotationSteps = int( RotationSteps + (8-(RotationSteps % 8)))	# Make it full circle

		# Start main loop
		for ie in xrange( RotationSteps ) :

			GPIO.output( StepPinsX, Seq[StepCounter] )

			StepCounter += StepDir

			# If we reach the end of the sequence
			# start again
			if (StepCounter>=StepCount) : StepCounter = 0
			if (StepCounter<0) : StepCounter = StepCount+StepDir
	
			# Wait before moving on
			time.sleep(WaitTime)

	except BaseException as e:
		print e
		GPIO.output(StepPinsX, (0,0,0,0))
		GPIO.cleanup()

# This method waits for button signal (see BUTTONpin). On the signal this method starts
# 	the main program. Point is to work properly with flag.
#
# Channel 	- pin of the button (this is system data)
def buttonAction ( channel ) :

	global KeepSilence

	# As here any other program running, ignore the signal
	if KeepSilence :
		return
		
	KeepSilence = True

	if channel == BUTTONpin and GPIO.input(channel) == 0 : # REPEAT program as it is
		runHeadProgram ()
	else :
		KeepSilence = False

	return

# Initialize main parameters: beeper state, head pins, repeat button, led matrix
def init () :

	# Load program
	global GlobalProgram
	GlobalProgram = readEntireProgram()
	
	# Set all pins as output
	global StepPinsX
	try :
		GPIO.setmode(GPIO.BCM)

				# JUST BELIEVE ME "TRUE" is the silence for beeper
		GPIO.setup(BEEPpin, GPIO.OUT)
		GPIO.output(BEEPpin, 1)

				# Just for head' reposition
		GPIO.setup(StepPinsX,GPIO.OUT)
		moveHead ()
		
				# Making button working
				# Which PIN see at start, we using pulling up, so this have to go down
				# NOTE: you will have a lot of signals if will have no Capacitor
				# 	see schema at project' wiki
		GPIO.setup(BUTTONpin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.add_event_detect(BUTTONpin, GPIO.FALLING, callback=buttonAction, bouncetime=1000)

	except :
		GPIO.cleanup()
		exit(1)

	# Init the screen
	LEDMatrix.init()
	LEDMatrix.clear_all()
	LEDMatrix.brightness(5)

# Cleaning up pins and params before exiting
def cleanUp () :
	global StepPinsX

	try :
		GPIO.output(StepPinsX, (0,0,0,0))
		updateDisplay ( "=" )
	finally :
		GPIO.cleanup()

# This method runs the program from main or by button signal
def runHeadProgram () :
	global GlobalProgram
	global KeepSilence
	
					# Please, only one in a once
	KeepSilence = True
	
					# Reading a program step by step and running it
	for str in GlobalProgram :
		if str[1] == 0:
			updateDisplay ( str[0], str[2] )
		else :
			updateDisplay ( str[0] )
			moveHead ( str[1], str[2], str[3] )

					# Well done, need to make all pins off
	GPIO.output(StepPinsX, (0,0,0,0))
	updateDisplay ( "=" )
	
					# Well, we are ready for button
	KeepSilence = False

# Uh, do nothing //
def main () :

	runHeadProgram ()
	return



if	__name__ == "__main__":
	init()
	main ()
	try :
		while True:
			time.sleep(0.3)
	except KeyboardInterrupt :
		print "Exiting"
	finally :
		cleanUp ()

## PAUL A KATUROV
## In ancient time used program by https://bitbucket.org/MattHawkinsUK/ still have pieces
##		in part of direction and matrix with magnets
